document.addEventListener('DOMContentLoaded', () => {
    const main = document.getElementById('main')

    var stages = document.createElement('div')
    stages.id = 'stageMenu'
    var stageHeader = document.createElement('div')
    stageHeader.className = 'stageHeader'

    var title = document.createElement('h2')
    title.innerText = 'Try Onet'

    var subTitle = document.createElement('h4')
    subTitle.innerText = 'Pick A Stage'
    
    stageHeader.appendChild(title)
    stageHeader.appendChild(subTitle)

    var row1 = document.createElement('span')
    row1.className = 'btnRow'
    var lvl1 = document.createElement('button')
    lvl1.innerText = '1'
    lvl1.id = 'lvl1'
    lvl1.className = 'stage'
    lvl1.addEventListener('click', () => {
        generator(1)
        stages.style.display = 'none'
    })

    var lvl2 = document.createElement('button')
    lvl2.id = 'lvl2'
    lvl2.disabled = true
    lvl2.innerHTML = '&#128274;'
    lvl2.className = 'stage locked'
    lvl2.addEventListener('click', () => {
        generator(2)
        stages.style.display = 'none'
    })

    var lvl3 = document.createElement('button')
    lvl3.innerHTML = '&#128274;'
    lvl3.id = 'lvl3'
    lvl3.disabled = true
    lvl3.className = 'stage locked'
    lvl3.addEventListener('click', () => {
        generator(3)
        stages.style.display = 'none'
    })

    var lvl4 = document.createElement('button')
    lvl4.innerHTML = '&#128274;'
    lvl4.id = 'lvl4'
    lvl4.disabled = true
    lvl4.className = 'stage locked'
    lvl4.addEventListener('click', () => {
        generator(4)
        stages.style.display = 'none'
    })

    var lvl5 = document.createElement('button')
    lvl5.innerHTML = '&#128274;'
    lvl5.id = 'lvl5'
    lvl5.disabled = true
    lvl5.className = 'stage locked'
    lvl5.addEventListener('click', () => {
        generator(5)
        stages.style.display = 'none'
    })

    var lvl6 = document.createElement('button')
    lvl6.innerHTML = '&#128274;'
    lvl6.id = 'lvl6'
    lvl6.disabled = true
    lvl6.className = 'stage locked'
    lvl6.addEventListener('click', () => {
        generator(6)
        stages.style.display = 'none'
    })

    row1.append(lvl1, lvl2, lvl3, lvl4, lvl5, lvl6)
    stages.appendChild(stageHeader)
    stages.appendChild(row1)
    main.appendChild(stages)

})