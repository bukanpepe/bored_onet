function generator(stage) {
    const field = document.getElementById('field')
    field.innerHTML = ''
    var board = document.createElement('div')
    board.id = 'board'

    var startBtn = document.createElement('button')
    startBtn.innerHTML = 'Start'
    startBtn.addEventListener('click', () => {
        countDown(settings.time)
        generateGame(settings.size)
        startBtn.style.display = 'none'
    })
    var settings = {
        size : null,
        layer : null,
        time : null
    }

    switch (stage) {
        case 1:
            settings.size = 6
            settings.layer = 1
            settings.time = 120
            break
        case 2:
            settings.size = 8
            settings.layer = 1
            settings.time = 120
            break
        case 3:
            settings.size = 10
            settings.layer = 1
            settings.time = 120
            break
    }

    function countDown(time) {
        var progress = document.createElement('div')
        progress.id = 'progress'
        var bar = document.createElement('div')
        bar.id = 'bar'

        var width = time
        var id = setInterval(frame, 1000)

        function frame() {
            if (time <= 0) {
                clearInterval(id)
                showAlert("Time's up!")
                for (let i = 0; i < document.getElementsByClassName('btn').length; i++) {
                    document.getElementsByClassName('btn')[i].disabled = true
                }

            } else {
                time--
                bar.style.width = `${((time/width)*100)}%`
                
                if (document.getElementsByClassName('btn').length === 0) {
                    showAlert("Congratulations! You win!")
                    document.getElementById(`lvl${stage + 1}`).disabled = false
                    document.getElementById(`lvl${stage + 1}`).classList.toggle('locked')
                    document.getElementById(`lvl${stage + 1}`).innerHTML = `${stage + 1}`
                    clearInterval(id)
                }
            }
        }
        progress.appendChild(bar)
        board.append(progress)
    }
    
    function generateGame(size) {
        var icons = ['&#9800;', '&#9801;', '&#9802;', '&#9803;', '&#9804;', '&#9805;', '&#9806;', '&#9807;', '&#9808;', '&#9809;', '&#9810;', '&#9811;', '&#10067;', '&#10071;', '&#127514;', '&#127535;', '&#127538;', '&#127539;', '&#127540;', '&#127541;', '&#127542;', '&#127799;', '&#127800;', '&#127801;', '&#127803;']

        var tot = size * size
        var iconUsed = tot / 4

        var generateThis = []

        for (let i = 0; i < size; i++) {
            var row = []
            generateThis.push(row)
        }

        var j = 0
        while (j < iconUsed) {
            var count = 0
            while (count < 4) {
                var x = Math.floor(Math.random() * size)
                generateThis[x].push(icons[j])
                count++
            }
            j++
        }
        for (let k = 0; k < generateThis.length; k++) {
            for (let i = generateThis[k].length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [generateThis[k][i], generateThis[k][j]] = [generateThis[k][j], generateThis[k][i]];
            }
        }

        for (let i = 0; i < generateThis.length; i++) {
            let row = document.createElement('p')
            row.className = 'row'
            for (let j = 0; j < generateThis[i].length; j++) {
                let btn = document.createElement('button')
                btn.type = 'button'
                btn.className = 'btn'
                btn.id = `${i}${j}`
                btn.innerHTML = generateThis[i][j]

                btn.addEventListener('click', () => {
                    btn.classList.toggle('active')
                    checkCounter()                    
                })
               
                row.appendChild(btn)
            }
            board.appendChild(row)
        }
    }
    
    function checkCounter() {
        let match = document.getElementsByClassName('btn active')

        if (match.length > 1) {            
            if (match[0].innerHTML !== match[1].innerHTML) {
                let i = 0
                match[0].style.background = 'crimson'
                match[1].style.background = 'crimson'
                setTimeout(() => {
                    match[0].style.background = ''
                    match[1].style.background = ''
                    
                    while (i != document.getElementsByClassName('btn active').length) {
                        document.getElementsByClassName('btn active')[i].classList.remove('active')
                    }
                }, 500)

            } else {
                let i = 0
                match[0].style.background = 'lawngreen'
                match[1].style.background = 'lawngreen'
                setTimeout(() => {
                    while (i != document.getElementsByClassName('btn active').length) {
                        document.getElementsByClassName('btn active')[i].remove()
                    }
                }, 500)
                
            }
        }
    }

    function showAlert(message) {
        let stageMenu = document.getElementById('stageMenu')

        let container = document.createElement('div')
        container.id = 'alertBox'
        let content = document.createElement('p')

        let btnContainer = document.createElement('span')
        btnContainer.id = 'alertClose'
        let closeBtn = document.createElement('a')
        closeBtn.innerText = 'X'
        closeBtn.addEventListener('click', e => {
            e.preventDefault()
            container.remove()
            board.remove()
            stageMenu.style.display = 'block'
        })
        
        content.innerText = message
           
        btnContainer.appendChild(closeBtn)
        container.appendChild(btnContainer)
        container.appendChild(content)
        document.getElementById('main').appendChild(container)
    }
    
    board.appendChild(startBtn)
    field.appendChild(board)

}