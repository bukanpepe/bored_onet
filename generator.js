document.addEventListener('DOMContentLoaded', () => {
    var mainBod = document.getElementById('main')

    var selectForm = document.createElement('form')
    selectForm.onsubmit = e => { e.preventDefault(); handleClick() }
    var selectInput = document.createElement('select')
    selectInput.type = 'select'
    selectInput.id = 'selectInput'
    selectInput.name = 'selectInput'
    var selectLabel = document.createElement('label')
    selectLabel.innerHTML = 'Select Board Size '

    var opt1 = document.createElement('option')
    opt1.text = '6x6'
    opt1.value = 6
    var opt2 = document.createElement('option')
    opt2.text = '8x8'
    opt2.value = 8
    var opt3 = document.createElement('option')
    opt3.text = '10x10'
    opt3.value = 10
    selectInput.options.add(opt1)
    selectInput.options.add(opt2)
    selectInput.options.add(opt3)

    var testBtn = document.createElement('button')
    testBtn.type = 'button'
    testBtn.addEventListener('click', () => handleClick())
    testBtn.innerText = 'Generate'
    // testBtn.onclick = handleClick()

    selectForm.appendChild(selectLabel)
    selectForm.appendChild(selectInput)
    selectForm.appendChild(testBtn)

    mainBod.appendChild(selectForm)

    var board = document.createElement('div')
    board.id = 'board'
    mainBod.appendChild(board)

    function handleClick() {

        var size = selectInput[selectInput.selectedIndex].value
        generateGame(size)
    }

    function generateGame(size) {
        board.innerHTML = ''
        var icons = ['&#9800;', '&#9801;', '&#9802;', '&#9803;', '&#9804;', '&#9805;', '&#9806;', '&#9807;', '&#9808;', '&#9809;', '&#9810;', '&#9811;', '&#10067;', '&#10071;', '&#127514;', '&#127535;', '&#127538;', '&#127539;', '&#127540;', '&#127541;', '&#127542;', '&#127799;', '&#127800;', '&#127801;', '&#127803;']

        var tot = size * size
        var iconUsed = tot / 4

        var generateThis = []

        for (let i = 0; i < size; i++) {
            var row = []
            generateThis.push(row)
        }

        var j = 0
        while (j < iconUsed) {
            var count = 0
            while (count < 4) {
                var x = Math.floor(Math.random() * size)
                generateThis[x].push(icons[j])
                count++
            }
            j++
        }
        for (let k = 0; k < generateThis.length; k++) {
            for (let i = generateThis[k].length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [generateThis[k][i], generateThis[k][j]] = [generateThis[k][j], generateThis[k][i]];
            }
        }

        for (let i = 0; i < generateThis.length; i++) {
            let row = document.createElement('p')
            row.className = 'row'
            for (let j = 0; j < generateThis[i].length; j++) {
                let btn = document.createElement('button')
                btn.type = 'button'
                btn.className = 'btn'
                btn.id = `${i}${j}`
                btn.innerHTML = generateThis[i][j]

                btn.addEventListener('click', () => {
                    btn.classList.toggle('active')
                    checkCounter()
                })
                row.appendChild(btn)
            }
            board.appendChild(row)
        }
    }

    function checkCounter() {
        let match = document.getElementsByClassName('btn active')

        if (match.length > 1) {
            if (match[0].innerHTML !== match[1].innerHTML) {
                let i = 0
                match[0].style.background = 'crimson'
                match[1].style.background = 'crimson'
                setTimeout(() => {
                    match[0].style.background = ''
                    match[1].style.background = ''
                    
                    while (i != document.getElementsByClassName('btn active').length) {
                        document.getElementsByClassName('btn active')[i].classList.remove('active')
                    }
                }, 500)

            } else {
                let i = 0
                match[0].style.background = 'lawngreen'
                match[1].style.background = 'lawngreen'
                setTimeout(() => {
                    while (i != document.getElementsByClassName('btn active').length) {
                        document.getElementsByClassName('btn active')[i].remove()
                    }
                }, 500)
                
                if (document.getElementsByClassName('btn').length === 0) {
                    showAlert("win")
                }
            }
        }
    }

    function showAlert(message) {

        let container = document.createElement('div')
        container.id = 'alertBox'
        let content = document.createElement('p')

        let btnContainer = document.createElement('span')
        let closeBtn = document.createElement('a')
        closeBtn.innerText = 'X'
        closeBtn.addEventListener('click', e => {
            e.preventDefault()
            container.remove()
        })

        if (message === 'win') {
            content.innerText = "Congratulations! You've cleared the board!"
            container.className = 'alertWin'
        }

        btnContainer.appendChild(closeBtn)
        container.appendChild(btnContainer)
        container.appendChild(content)
        mainBod.appendChild(container)
    }
})
